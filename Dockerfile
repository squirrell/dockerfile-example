FROM centos:centos7

EXPOSE 8080

COPY index.html.tmpl /var/run/web/index.html.tmpl

CMD cd /var/run/web &&  sed s/frmHost/$(hostname)/ index.html.tmpl > index.html && rm -f index.html.tmpl && python -m SimpleHTTPServer 8080
