# Dockerfile Example

1. Login to the  [Katacoda training enviornment](https://www.katacoda.com/openshift/courses/playgrounds/openshift311)

1. Build App

        oc  new-project myproj &&\
        oc new-app --strategy docker --name myapp \
           https://bitbucket.org/squirrell/dockerfile-example.git &&\
        oc expose service myapp

1. Get the route to the App

        myroute=`oc get routes | grep -oh "myapp-myproj.*com"` &&\
        echo $myroute

1. View the App in a browser.  Notice the pod name displayed. This will become useful later.

1. Save save URL to Bitbucket

1. Loop a curl over the App

        while true; do curl -s $myroute | grep -oh ': .*<'; sleep 2; done
   Notice the pod name stays the same, so it is being serverd by just one Pod / Web Server.
   
1. Open another terminal and scale up the app

    Notice there is only 1 application pod running prior to running the oc scale command and 3 afterwards

        oc get pods -o wide &&\
        oc scale dc myapp --replicas=3 &&\
        sleep 5 &&\
        oc get pods -o wide

1. Return to the first terminal window and confirm that 3
servers are now serving up the _Hello World_ web page

1. While in the 60 minute window you can view the App at [Katacoda Web App](http://myapp-myproj.2886795275-80-kitek03.environments.katacoda.com/)
