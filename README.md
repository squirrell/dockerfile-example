# Dockerfile Example

1. Start a [Katacoda training enviornment](https://www.katacoda.com/openshift/courses/playgrounds/openshift311).
This example works well with the training cluster at Katacoda but
your mileage may vary when using this example with other OpenShift clusters as the **_Dockerfile_** build strategy 
requires access to the docker socket. The security implications of having it enabled are serious especially in 
shared environments like [OpenShift Online](https://www.openshift.com/) where this example will fail.

2. Login to the OpenShift cluster.  Click the "Start Scenario" button and then the oc login
command with the black background.  This will paste the command into the terminal window.  Type "y" 
to accept the insecure connection 

    This will be different if you are not using Katacoda. For more information refer to the 
example oc login command below and your cluster documentation.
    
        oc login -u developer -p developer  <<clusterURL>>

3. Create a new project for this example.

        oc  new-project myproj
       
4. Create the docker application.
    
        oc new-app --strategy docker --name myapp https://bitbucket.org/squirrell/dockerfile-example.git
        
5. Expose the application out the OCP cluster.
    
        oc expose service myapp

6. List the URI that was exposed in the previous command. (HOST/PORT field).
    
        oc get routes

7. Open the above route/URL in a new web browser window.
The _Hello World_ web page should show along with the hostname of the container
running the web sever.

8. Run the below command from the terminal which only displays 
the hostname of the container serving the web page.
    
        myroute=`oc get routes | grep -oh "myapp-myproj.*com"` &&\
        while true; do curl -s <<webRoute>> | grep -oh ': .*<'; sleep 2; done

    Confirm that only one server is serving up the _Hello World_ 
    application. Leave this running so it can display 
    the hostnames of the servers
    after scaling myapp up to more than one server.

9. Scale up myapp to 3 servers. By opening a new terminal 
(click the + tab), and running the following command.

   Notice there is only 1 application pod running prior to running the oc scale command and
    3 afterwards.

        oc get pods -o wide &&\
        oc scale dc myapp --replicas=3 &&\
        sleep 5 &&\
        oc get pods -o wide

10. Return to the first terminal window and confirm that 3
servers are now serving up the _Hello World_ web page.
